export interface loginUser{
  account:string|number,
  password:string
}
interface  account {
  type?:string,
  message:string,
  required?:boolean,
  trigger:string
}
interface password  extends  account{

}
interface accountLength {
  min:number,
  max:number,
  message:string,
  trigger:string
}
interface passLength extends  accountLength{

}
export interface rules {
  account:Array<account|accountLength>,
  password:Array<password|passLength>
}






