export interface RegisterUser {
  name: string,
  account: string,
  password: string,
  password2: string,
  role: string;
}
interface content{
  message:string,
  required?: boolean,
  trigger: string
}
interface Length {
  min: number,
  max: number,
  message: string,
  trigger: string,
}
interface account {
  type: string,
  message: string
  required?: boolean
  trigger: string,
}
interface validator{
  validator: (rule: RegisterRules, value: string, callback: any) => void,
  trigger: string
}
export interface RegisterRules{
  name:Array<content|Length>,
  account:Array<account>,
  password:Array<content|Length>,
  password2:Array<content|Length|validator>
}




