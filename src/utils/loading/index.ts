import {ElLoading} from 'element-plus'
let handleLoading: any
const startLoading=()=>{
    handleLoading=ElLoading.service({
        target:document.querySelector('.app-main') as HTMLElement,
        lock: true,
        text: 'Loading',
        spinner: 'el-icon-loading',
        background: 'aliceblue'
    })
}
const endLoading=()=>{
    handleLoading.close()
}
export {
    startLoading,
    endLoading
}
