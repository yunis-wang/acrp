
import {Workbook,Worksheet,Buffer} from 'exceljs'
interface columnType{
    header:object,
    options:object
}
class exportExcel{
    // 操作Excel的实例
    public wb:Workbook
    // Excel中的表格
    public sheet:Worksheet
    // Excel的表头
    public column:Array<columnType>
    // 下载Excel的buffer实例
    public buffer:Buffer
    // 文件名
    public fileName:string|null
    constructor(column:Array<columnType>,fileName:string|null) {
        // 创建操作Excel的实例
        this.wb=new Workbook()
        // 创建一个名为sheet的表格
        this.sheet=this.wb.addWorksheet('sheet')
        // 初始化文件名
        this.fileName=fileName
        // 获取待生成Excel的数据
        this.column=column
        // 设置表头
        this.setHeader()
        // 设置表头文本居中
        this.setHeaderAlign()
        // 设置表项下拉项内容
        this.setOptions()
    }
    // 将item的header属性映射成一个数组，用于定义Excel表头
    public setHeader=()=>this.sheet.columns=this.column.map(item=>item.header)
    // 通过ASCLL码生成对应字母，进而给表头的每一项设置居中属性
    public setHeaderAlign=()=>{
        this.column.forEach((item,index)=>{
            this.sheet.getCell(String.fromCharCode(65+index)+1).alignment = { vertical: 'middle', horizontal: 'center' };
        })
    }
    // 设置表项下拉列表
    public setOptions=()=>{
        // 创建250行带有下拉列表的表项
        for (let i = 0; i < 500; i++) {
            const row =  i + 2;
            this.column.forEach((item,index)=>{
                // 设置下拉列表内容
                this.sheet.getCell(row, index+1).dataValidation = {
                    type: 'list',
                    allowBlank: true,
                    formulae: [`"${item.options}"`]
                };
                // 设置表项内容居中
                this.sheet.getCell(String.fromCharCode(65+index)+row).alignment = { vertical: 'middle', horizontal: 'center' };
            })
        }
        this.download()
    }
    // 下载Excel文件
    public download=async ()=>{
        // 获取表格内容
        this.buffer=await this.wb.xlsx.writeBuffer()
        // 创建blob对象
        let blob = new Blob([this.buffer], {
            type: "application/octet-stream"
        });
        let url: Blob|string = blob, saveName = `${this.fileName||'template'}.xlsx`;

        if (typeof url == 'object' && url instanceof Blob) {
            url = URL.createObjectURL(url); // 创建blob地址
        }
        // 创建a标签，用于下载文件
        let aLink = document.createElement('a');
        aLink.href = url;
        aLink.download = saveName || ''; // HTML5新增的属性，指定保存文件名，可以不要后缀，注意，file:///模式下不会生效
        let event=null;
        if (window.MouseEvent) event = new MouseEvent('click');
        else {
            event = document.createEvent('MouseEvents');
            event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0,
                null);
        }
        // 触发a标签的点击事件，从之前定义的blob地址获取二进制流
        aLink.dispatchEvent(event);
        // 移除blob地址
        URL.revokeObjectURL(url)
    }
}

export default exportExcel
