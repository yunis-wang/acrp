import { ElMessage } from "element-plus"

// 消息
const Message = (message: string): any => {
  return ElMessage({
    showClose: true,
    message,
    center:true,
    duration:1500
  })
}

// 成功
const successMessage = (message: string): any => {
  return ElMessage({
    showClose: true,
    message,
    type: "success",
    center:true,
    duration:1500
  })
}

// 警告
const warnMessage = (message: string): any => {
  return ElMessage({
    showClose: true,
    message,
    type: "warning",
    center:true,
    duration:1500
  })
}

// 失败
const errorMessage = (message: string): any => {
  return ElMessage({
    showClose: true,
    message,
    type: "error",
    center:true,
    duration:1500
  })
}

export {
  Message,
  successMessage,
  warnMessage,
  errorMessage
}
