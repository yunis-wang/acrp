import { App } from "vue";
import {
    ElUpload,
    ElBreadcrumb,
    ElBreadcrumbItem,
    ElSubmenu,
    ElButton,
    ElCol,
    ElRow,
    ElSpace,
    ElCard,
    ElDropdown,
    ElDialog,
    ElMenu,
    ElMenuItem,
    ElDropdownItem,
    ElDropdownMenu,
    ElIcon,
    ElInput,
    ElForm,
    ElFormItem,
    ElLoading,
    ElPopover,
    ElPopper,
    ElTooltip,
    ElSelect,
    ElOption,
    ElCheckboxGroup,
    ElCheckbox,
    ElScrollbar,
    ElSkeleton,
    ElTable,
    ElTableColumn,
    ElAlert,
    ElDivider,
    ElAffix,
    ElTabs,
    ElTabPane,
    ElTag,
    ElRadio,
    ElRadioGroup,
    ElDatePicker
} from "element-plus";

const components = [
    ElDatePicker,
    ElRadio,
    ElRadioGroup,
    ElTag,
    ElTabs,
    ElTabPane,
    ElAffix,
    ElDivider,
    ElAlert,
  ElTableColumn,
  ElTable,
  ElScrollbar,
  ElUpload,
  ElSkeleton,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElSubmenu,
  ElButton,
  ElCol,
  ElRow,
  ElSpace,
  ElCard,
  ElDropdown,
  ElDialog,
  ElMenu,
  ElMenuItem,
  ElDropdownItem,
  ElDropdownMenu,
  ElIcon,
  ElInput,
  ElForm,
  ElFormItem,
  ElPopover,
  ElPopper,
  ElTooltip,
  ElSelect,
  ElOption,
  ElCheckboxGroup,
  ElCheckbox
];

const plugins = [ElLoading];

export function useElementPlus(app: App) {
  components.forEach((component) => {
    app.component(component.name, component);
  });
  plugins.forEach((plugin) => {
    app.use(plugin);
  });
}
