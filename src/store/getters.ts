const getters = {
  sidebar: (state: unknown) => state.app.sidebar,
  device: (state: unknown) => state.app.device,
  accessibleRoutes: (state: unknown)=> state.authority.accessibleRoutes
}

export default getters
