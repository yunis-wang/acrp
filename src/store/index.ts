import { createStore } from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import authority from "./modules/authority";
export default createStore({
  getters,
  modules: {
    app,
    settings,
    authority
  }
})
