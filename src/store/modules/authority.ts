import {storageSession} from "/@/utils/storage";
const state={
    accessibleRoutes:storageSession.getItem('routes')||[]
}
const mutations={
    // 此处state不能解构赋值，否则vuex无法监听到state的变化，进而数据更新不能及时刷新页面
    FILTER_ROUTE(state,data){
        state.accessibleRoutes=data
        storageSession.setItem('routes',data)
    }
}
const actions={
    filterRoute({commit},data){
        commit('FILTER_ROUTE',data)
    }
}
export default {
    namespaced:true,
    state,
    actions,
    mutations
}
