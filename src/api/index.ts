
import { http } from "../utils/http"
import {AxiosRequestConfig} from "axios";
const postConfig={
    transformRequest: [function (data) {
        // Do whatever you want to transform the data
        let ret = ''
        for (let item in data) {
            // 如果要发送中文 编码
            ret += encodeURIComponent(item) + '=' + encodeURIComponent(data[item]) + '&'
        }
        return ret
    }],
    headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
export type dataType=string|object|Array<any>
// 用户登录
export const getLogin = (data: dataType): Promise<dataType>=>  http.post("/acrpUser/userlogin", data, postConfig)
// 上传文件
export const upload=(data:dataType):Promise<dataType>=>  http.post('/excelWriter/upload',data)
// 获取模板数据
export const getTemplateData= (data?:dataType):Promise<dataType>=>  http.get('/acrpCtJclb/selectall')
// 获取配置要素列表
export const getOptionsData= (data?:dataType):Promise<dataType>=>  http.get('/acrpCtJclb/selectallAndId')
// 修改要素列表
export const updateOptionsData= (data?:dataType):Promise<dataType>=>  http.request('put','/acrpCtJclb/stopandstart',data as AxiosRequestConfig)
// 新增要素列表
export const addOptionsData= (data?:dataType):Promise<dataType>=>  http.request('put','/acrpCtJclb/ctinsert',data as AxiosRequestConfig)
// 查询信息
export const queryInformation=(data?:dataType):Promise<dataType> => http.request('put','/acrpQuScn/queryAll',data as AxiosRequestConfig)
// 下载查询结果
export const downloadInformation=(data?:dataType):Promise<dataType> => http.request('put','/UDload/download',data as AxiosRequestConfig,{
    responseType:'blob'
})
// 获取用户可选择字段
export const getChartOptionsData=(data?:dataType):Promise<dataType> => http.get('/DataAnalysis/option')
// 获取图表数据
export const getChartData=(data?:dataType):Promise<dataType> => http.request('put','/DataAnalysis/statisticalResult',data as AxiosRequestConfig)


