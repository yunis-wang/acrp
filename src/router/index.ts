import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Layout from "../layout/index.vue";
import ErrorPage from '../views/error/404.vue'
import { storageSession } from "../utils/storage";
import NProgress from "../utils/progress";

const routes: Array<RouteRecordRaw> = [
  {
    path:'/',
    name:'home',
    component:Layout,
    redirect:'/welcome',
    children:[
      {
        path:'/welcome',
        name:'welcome',
        component:()=>import('../views/welcome.vue'),
        meta:{
          icon:'el-icon-s-home',
          title:'首页',
          showLink: true,
          savedPosition: false
        }
      }
    ],
    meta:{
      icon: "el-icon-s-home",
      showLink: true,
      savedPosition: false
    }
  },
  {
    path:'/template',
    name:'template',
    component:Layout,
    redirect:'/template/formulate',
    children:[
      {
        path:'/template/formulate',
        name:'formulate',
        component:()=>import('../views/formulate/index.vue'),
        meta:{
          icon:'el-icon-download',
          title:'生成模板',
          showLink: true,
          savedPosition: false
        }
      },
      {
        path:'/template/upload',
        name:'upload',
        component:()=>import('../views/upload/index.vue'),
        meta:{
          icon: "el-icon-upload",
          title:'上传文件',
          showLink: true,
          savedPosition: false
        }
      }
    ],
    meta:{
      icon: "el-icon-menu",
      title:"模板下载与上传",
      showLink: true,
      savedPosition: false
    }
  },
  {
    path:'/query',
    name:'query',
    component:Layout,
    redirect:'/query/index',
    children:[
      {
        path:'/query/index',
        name:'query-view',
        component:()=>import('../views/query/index.vue'),
        meta:{
          icon: "el-icon-search",
          title:'查询信息',
          showLink: true,
          savedPosition: false
        }
      }
    ],
    meta:{
      icon: "el-icon-search",
      title:'查询信息',
      showLink: true,
      savedPosition: false
    }
  },
  {
    path:'/options',
    name:'options',
    component:Layout,
    redirect:'/options/index',
    children:[
      {
        path:'/options/index',
        name:'options-view',
        component:()=>import('../views/options/index.vue'),
        meta:{
          icon: "el-icon-setting",
          title:'配置下拉列表',
          showLink: true,
          savedPosition: false
        }
      }
    ],
    meta:{
      icon: "el-icon-setting",
      title:'配置管理',
      showLink: true,
      savedPosition: false
    }
  },
  {
    path:'/chart',
    name:'chart',
    component:Layout,
    redirect:'/chart/index',
    children:[
      {
        path:'/chart/index',
        name:'chart-view',
        component:()=>import('../views/chart/index.vue'),
        meta:{
          icon: "el-icon-pie-chart",
          title:'生成图表',
          showLink: true,
          savedPosition: false
        }
      }
    ],
    meta:{
      icon: "el-icon-pie-chart",
      title:'图表管理',
      showLink: true,
      savedPosition: false
    }
  },
  // {
  //   // 系统管理模块路由
  //   path: "/system",
  //   name: "system",
  //   component: Layout,
  //   redirect: "/system/base",
  //   children: [
  //     {
  //       path: "/system/base",
  //       component: () =>
  //         import(/* webpackChunkName: "system" */ "../views/system/user.vue"),
  //       meta: {
  //         // icon: '',
  //         title: "基础信息",
  //         showLink: false,
  //         savedPosition: true,
  //       },
  //     },
  //     {
  //       path: "/system/dict",
  //       component: () =>
  //         import(/* webpackChunkName: "system" */ "../views/system/dict.vue"),
  //       meta: {
  //         // icon: 'el-icon-setting',
  //         title: "字典管理",
  //         showLink: false,
  //         savedPosition: true,
  //       },
  //     },
  //   ],
  //   meta: {
  //     icon: "el-icon-setting",
  //     title: "系统管理",
  //     showLink: true,
  //     savedPosition: true,
  //   },
  // },
  {
    // 登录页面路由
    path: "/login",
    name: "login",
    component: () => import(/* webpackChunkName: "login" */ "../views/login.vue"),
    meta: {
      title: "登录",
      showLink: false,
    },
  },

  {
    // 找不到路由重定向到404页面
    path: "/:pathMatch(.*)",
    component:ErrorPage,
    // component: Layout,
    // redirect: "/error/404",
    meta: {
      icon: "el-icon-s-home",
      title: "404",
      showLink: false,
      savedPosition: false,
    }
  },
  {
    // 重定向到当前路由，局部刷新页面
    path: "/redirect",
    component: Layout,
    children: [
      {
        path: "/redirect/:path(.*)",
        component: () => import("../views/redirect.vue"),
      },
    ],
    meta: {
      icon: "el-icon-s-home",
      title: "首页",
      showLink: false,
      savedPosition: false,
    }
  },
];

const router = createRouter({
  // 创建哈希路由，即路由中会多一个#
  history: createWebHashHistory(),
  routes,
  /**
   *
   * @param to  即将跳转到的路由所对应的信息对象
   * @param from  当前路由对象
   * @param savedPosition 当且仅当 popstate 导航 (通过浏览器的 前进/后退 按钮触发) 时才可用
   */
  scrollBehavior(to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      // 当通过浏览器的前进后退按钮跳转按钮时，页面滚动到上次点击浏览器前进后退按钮时，所处的页面位置
      if (savedPosition) {
        return savedPosition;
      } else {
        // 如果当前路由对象中的saveScrollTop值存在，在切换到新的路由时
        // 新路由页面会自动滚到当前路由页面所处的位置
        if (from.meta.saveSrollTop) {
          const top: number =
            document.documentElement.scrollTop || document.body.scrollTop;
          resolve({ left: 0, top });
        }
      }
    });
  },
});

// 初始化默认可访问路由的数组
const whiteList = ["/login"];
/**
 * @param to 即将跳转到的路由信息对象
 * @param _form 当前路由对象
 * @param next 控制路由访问的开关
 */
// 路由跳转前的拦截控制
router.beforeEach((to, _from, next) => {
  // 开启页面顶部的小进度条
  NProgress.start();
  // 动态title
  document.title = to.meta.title as string;
  // 如果访问的路由是默认可访问路由数组中的内容，则直接放行，可直接访问对应路由
  // 如果访问的路由不是其中的内容，则需要判断sessionStorage中是否存在info这一键名所对应的值
  // 若该值存在，那么直接放行，直接访问对应路由，否则重定向到/login路由
  whiteList.indexOf(to.path) !== -1 || storageSession.getItem("info")
    ? next()
    : next("/login"); // 全部重定向到登录页
});
// 路由跳转后的拦截控制
router.afterEach(() => {
  // 关闭顶部小进度条
  NProgress.done();
});

export default router;
