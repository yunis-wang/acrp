type ProxyItem = Array<string>;

type ProxyList = ProxyItem[];

// 将字符串中的指定内容替换成为''
const regExps = (value: string,reg: string): string => {
  return value.replace(new RegExp(reg, 'g'), '');
}

/**
 *
 * @param list 二维数组，二维中的每一个一维数组存放的待处理路由前缀和目标接口地址
 */
export function createProxy(list: ProxyList = []) {
  const ret: any = {};
  for (const [prefix, target] of list) {
    // 迭代二维数组，动态生成跨域代理配置
    ret[prefix] = {
      // 目标接口地址
      target: target,
      // 换源
      changeOrigin: true,
      // 重写接口访问路径
      rewrite: (path:string) => regExps(path, prefix)
    };
  }
  return ret;
}
