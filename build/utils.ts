import dotenv from 'dotenv';
// 按需加载不同的环境变量文件

// 定义环境变量对象接口
export interface ViteEnv {
  VITE_PORT: number;
  VITE_OPEN: boolean;
  VITE_USE_MOCK: boolean;
  VITE_PUBLIC_PATH: string;
  VITE_PROXY: Array<string[]>
}

export function loadEnv(): ViteEnv {
  // 获取当前所处环境
  const env = process.env.NODE_ENV||'development';
  const ret: any = {};
  const envList = [ '.env.local', '.env',`.env.${env}.local`, `.env.${env}`]
  // 加载环境变量文件
  envList.forEach((e) => {
    dotenv.config({
      path: e,
    });
  });
  // 迭代环境变量对象
  for (const envName of Object.keys(process.env)) {
    let realName = (process.env as any)[envName].replace(/\\n/g, '\n');
    // 当realName为'true'或者'false'字符串时，转换为对应的布尔值，否则realName值为其本身
    realName = realName === 'true' ? true : realName === 'false' ? false : realName;
    if (envName === 'VITE_PORT') {
      // 当键名为端口号时，将键值转换为数字类型
      realName = Number(realName);
    }
    if (envName === 'VITE_OPEN') {
      // 当键名为控制是否自动打开浏览器的属性时，将键值转换为布尔类型
      realName = Boolean(realName);
    }
    if (envName === 'VITE_PROXY') {
      // 当键名为代理配置时，将键值转换为对象
      try {
        realName = JSON.parse(realName);
      } catch (error) { }
    }
    // 修改向外暴露的配置对象
    ret[envName] = realName;
    // 修改全局环境对象中的值
    process.env[envName] = realName;
  }
  return ret;
}

